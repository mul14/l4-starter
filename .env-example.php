<?php

return [
	'DEBUG' => false,
	'BASE_URL' => 'http://localhost',
	'APP_KEY' => 'vTxtnPKgzrX9SojJ1wDm8cw88ZDhRl84',
	'CACHE_DRIVER' => 'file',

	'MYSQL_HOSTNAME' => 'localhost',
	'MYSQL_DATABASE' => 'homestead',
	'MYSQL_USERNAME' => 'homestead',
	'MYSQL_PASSWORD' => 'secret',

	'MAIL_DRIVER'       => 'log', // smtp, mail, sendmail, mailgun, mandrill
	'MAIL_HOST'         => '127.0.0.1', // smtp.gmail.com
	'MAIL_PORT'         => 1025, // 587, 445
	'MAIL_FROM_ADDRESS' => '',
	'MAIL_FROM_NAME'    => '',
	'MAIL_ENCRYPT'      => false // tls
	'MAIL_SMTP_USERNAME' => null,
	'MAIL_SMTP_PASSWORD' => null,

	'SESSION_DRIVER' => 'file',
	'QUEUE_DRIVER' => 'sync',
];
